const parse = require('csv-parse');
const fs = require('fs');
const { finished } = require('stream/promises');

const processFile = async () => {
    const records = new Set();
    const parser = fs
        .createReadStream(`${__dirname}/test.csv`)
        .pipe(parse({}));
    parser.on('readable', function(){
        let record;
        while (record = parser.read()) {
            if (['skipped', 'failed'].includes(record[0])) {
                const name = record[7].split('.');
                records.add(name[name.length - 1]);
            }
        }
    });
    await finished(parser);
    return records
}

(async () => {
    const records = await processFile()
    console.info(Array.from(records).join(','));
})()